﻿using System;
using System.Text;
using System.Collections.Generic;
namespace HuffmanAlgo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;
            string input;
            do
            {
                Console.WriteLine("Введіть рядок: "); input = Console.ReadLine();
            } while (input.Length <= 1);
            List < Node > list = SortedLetters(input);
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~");
            for(int i=0; i<list.Count; i++)
                Console.WriteLine($"Символ \"{list[i].val}\" : {list[i].weight}");
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~");
            Tree tree = new Tree(list);
            Console.WriteLine("Поетапне виведення вузлів списку і їх частот:");
            tree.MakeTree();
            List<Node> codes = tree.GetCodes();
            for (int i = 0; i < codes.Count; i++)
                Console.WriteLine($"Буква {codes[i].val} - код {codes[i].code}");
        }
        public class Node
        {
            public char val = '\0'; //символ
            public string code = ""; //код
            public bool visited = false; //чи відвіданий
            public int weight; //частота
            public Node leftancestor; //лівий предок
            public Node rightancestor; //правий предок
            public Node()
            {
                leftancestor = null;
                rightancestor = null;
                weight = 1;
            }
            public Node(char val)
            {
                leftancestor = null;
                rightancestor = null;
                this.val = val;
                weight = 1;
            }
            public Node(int weight, Node leftancestor, Node rightancestor)
            {
                this.weight = weight;
                this.leftancestor = leftancestor;
                this.rightancestor = rightancestor;
            }
        }
        public class Tree
        {
            public List<Node> list;
            public Tree(List<Node> list)
            {
                this.list = list;
            }
            public void MakeTree() //збір дерева
            {
                while (list.Count != 1)
                    MakeBranch();
            }
            public void MakeBranch() //перетворення двох вузлів з найменшною частотою в гілку
            {
                Node oneminnode = list[0]; int onemini = 0;
                for (int i = 1; i < list.Count; i++)
                    if (oneminnode.weight > list[i].weight)
                    {
                        oneminnode = list[i];
                        onemini = i;
                    }
                Node twominnode; int twomini = 0, k = 0;
                if (onemini != 0)
                {
                    twominnode = list[0];
                    twomini = 0;
                    k = 0;
                }
                else
                {
                    twominnode = list[1];
                    twomini = 1;
                    k = 1;
                }
                for (int i = k; i < list.Count; i++)
                {
                    if (i == onemini) continue;
                    if (list[i].weight < twominnode.weight)
                    {
                        twominnode = list[i];
                        twomini = i;
                    }
                }
                for (int i=0; i<list.Count; i++)
                {
                    Console.WriteLine($"list[{i}] : weight: {list[i].weight}");
                }
                Console.WriteLine();
                Node pair = new Node();
                    if (onemini < twomini)
                    {
                        list.Remove(list[onemini]);
                        list.Remove(list[twomini - 1]);
                    }
                    else
                    {
                        list.Remove(list[twomini]);
                        list.Remove(list[onemini - 1]);
                    }
                if (list.Count == 0)
                {
                    if (onemini < twomini)
                        pair = new Node(oneminnode.weight + twominnode.weight, twominnode, oneminnode);
                    else if (onemini > twomini)
                        pair = new Node(oneminnode.weight + twominnode.weight, oneminnode, twominnode);
                    list.Add(pair);
                }
                else
                {
                    if (onemini < twomini)
                    {
                        pair = new Node(oneminnode.weight + twominnode.weight, twominnode, oneminnode);
                        list.Insert(twomini - 1, pair);
                    }
                    else if (onemini > twomini)
                    {
                        pair = new Node(oneminnode.weight + twominnode.weight, oneminnode, twominnode);
                        list.Insert(onemini - 1, pair);
                    }
                }
               
            }
            public List<Node> GetCodes() //знаходження коду кожного символа
            {
                List<Node> codelist = new List<Node>();
                while ((!list[0].leftancestor.visited && !list[0].rightancestor.visited)
                    || (list[0].leftancestor.visited && !list[0].rightancestor.visited)
                    || (!list[0].leftancestor.visited && list[0].rightancestor.visited))
                {
                    Node node = RunOver(list[0]);
                    if (node.val != '\0')
                        codelist.Add(node);
                }
                return codelist;
            }
            public Node RunOver(Node node) //пробіг по предкам
            {
                Queue<int> queue = new Queue<int>();
                string code ="";
                while (node.val == '\0' && !node.visited)
                {
                    if (node.rightancestor.visited && node.leftancestor.visited)
                    {
                        node.visited = true;
                        break;
                    }
                    if (!node.rightancestor.visited)
                    {
                        node = node.rightancestor;
                        queue.Enqueue(0);
                    }
                    else
                    {
                        node = node.leftancestor;
                        queue.Enqueue(1);
                    }
                }
                node.visited = true;
                if (node.val != '\0')
                    while (queue.Count != 0)
                        code += queue.Dequeue().ToString();
                node.code = code;
                return node;
            }
        }
        static List<Node> SortedLetters(string input) //список з символів, що зустрічаються, відсортований
        {
            List<Node> list = new List<Node>();
            for (int i = 0; i < input.Length; i++)
            {
                bool IfAdd = true;
                if (i != 0)
                {
                    for (int j = 0; j < list.Count; j++)
                        if (list[j].val == input[i])
                        {
                            list[j].weight++;
                            IfAdd = false;
                            break;
                        }
                }
                if (IfAdd) list.Add(new Node(input[i]));
            }
            bool notsorted = true;
            while (notsorted)
            {
                notsorted = false;
                for (int i = 1; i < list.Count; i++)
                { 
                    if (list[i-1].weight > list[i].weight)
                    {
                        notsorted = true;
                        Node tmp = list[i];
                        list[i] = list[i - 1];
                        list[i - 1] = tmp;
                    }
                }
            }
            return list;
        }
    }
    
}
